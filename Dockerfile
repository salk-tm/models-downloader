FROM mambaorg/micromamba:1.5.1

USER root
RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    && rm -rf /var/lib/apt/lists/*
USER $MAMBA_USER

COPY --chown=$MAMBA_USER:$MAMBA_USER environment.yml /tmp/environment.yml
RUN micromamba install -y -n base -f /tmp/environment.yml && \
    micromamba clean --all --yes

WORKDIR /workspace
COPY ./src /workspace/src

# Set the PYTHONPATH environment variable to include the parent directory of the package
# This helps imports
ENV PYTHONPATH=/workspace:$PYTHONPATH