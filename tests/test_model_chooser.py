import json
import pandas as pd
import pytest

from pathlib import Path

from src.model_chooser import (
    get_age,
    get_mode,
    get_params,
    get_primary_model_id,
    get_species,
    determine_primary_model_id,
    determine_lateral_model_id,
    determine_crown_model_id,
)


@pytest.fixture
def primary_model_id_present():
    """Primary model id present in the model parameters."""
    params = {
        "species": None,
        "mode": None,
        "age": None,
        "lateral_model_id": None,
        "primary_model_id": "123456",
        "crown_model_id": None,
    }
    return params


@pytest.fixture
def primary_model_and_dataset_params_present():
    """Primary model id and dataset parameters present in the model parameters."""
    params = {
        "species": "canola",
        "mode": "cylinder",
        "age": "7",
        "lateral_model_id": None,
        "primary_model_id": "models/canola_pennycress_arabidopsis/primary",
        "crown_model_id": None,
    }
    return params


@pytest.fixture
def invalid_primary_model_and_dataset_params_present():
    """Primary model id and dataset parameters present in the model parameters."""
    params = {
        "species": "canola",
        "mode": "cylinder",
        "age": "7",
        "lateral_model_id": None,
        "primary_model_id": "123456",
        "crown_model_id": None,
    }
    return params


@pytest.fixture
def real_valid_species(model_chooser):
    """Valid species list in the model parameters."""
    valid_species = model_chooser["species"].unique().tolist()
    return valid_species


@pytest.fixture
def real_valid_modes(model_chooser):
    """Valid mode list in the model parameters."""
    valid_modes = model_chooser["mode"].unique().tolist()
    return valid_modes


@pytest.fixture
def real_valid_ages(model_chooser):
    """Valid age list in the model parameters."""
    valid_ages = model_chooser["age"].unique().tolist()
    return valid_ages


@pytest.fixture
def dummy_valid_species():
    return ["species1", "species2"]


@pytest.fixture
def dummy_valid_mode():
    return ["mode1", "mode2"]


@pytest.fixture
def dummy_valid_age():
    return ["young", "mature"]


@pytest.fixture
def model_chooser_real_df():
    data = {
        "species": [
            "soybean",
            "canola",
            "pennycress",
            "arabidopsis",
            "arabidopsis",
            "arabidopsis",
            "rice",
            "rice",
        ],
        "mode": [
            "cylinder",
            "cylinder",
            "cylinder",
            "multiplant cylinder",
            "cylinder",
            "plate",
            "cylinder",
            "cylinder",
        ],
        "age": [
            "2, 3, 4, 5, 6, 7, 8",
            "2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13",
            "2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14",
            "2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14",
            "2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14",
            "7, 8, 9, 10, 11, 12, 13, 14",
            "2, 3, 4, 5",
            "6, 7, 8, 9, 10",
        ],
        "primary_model_id": [
            "models/soybean/primary",
            "models/canola_pennycress_arabidopsis/primary",
            "models/canola_pennycress_arabidopsis/primary",
            "models/canola_pennycress_arabidopsis/primary",
            "models/canola_pennycress_arabidopsis/primary",
            "models/arabidopsis_plates/primary",
            "models/rice/younger/primary",
            None,
        ],
        "lateral_model_id": [
            "models/soybean/lateral",
            "models/canola/lateral",
            "models/canola_pennycress_arabidopsis/lateral",
            "models/canola_pennycress_arabidopsis/lateral",
            "models/canola_pennycress_arabidopsis/lateral",
            "models/arabidopsis_plates/lateral",
            None,
            None,
        ],
        "crown_model_id": [
            None,
            None,
            None,
            None,
            None,
            None,
            "models/rice/younger/crown",
            "models/rice/older/crown",
        ],
        "primary_name": [
            "primary",
            "primary",
            "primary",
            "primary",
            "primary",
            "primary",
            "primary",
            None,
        ],
        "lateral_name": [
            "lateral",
            "lateral",
            "lateral",
            "lateral",
            "lateral",
            "lateral",
            None,
            None,
        ],
        "crown_name": [None, None, None, None, None, None, "crown", "crown"],
    }
    return pd.DataFrame(data)


@pytest.fixture
def dataset_params():
    data = {
    "species": "rice",
    "mode": "cylinder",
    "age": "3",
    "lateral_model_id": None,
    "primary_model_id": None,
    "crown_model_id": None,
}
    return data


def test_get_params_valid_file(input_dir):
    result = get_params(input_dir)
    expected_result = {
        "species": "canola",
        "mode": "cylinder",
        "age": "7",
        "lateral_model_id": None,
        "primary_model_id": None,
        "crown_model_id": None,
    }
    assert (
        result == expected_result
    ), "The function should correctly read and return the JSON data."


def test_get_params_invalid_file():
    with pytest.raises(FileNotFoundError):
        get_params("invalid_input_dir")


def test_choose_species(input_dir, real_valid_species):
    params = get_params(input_dir)
    choosen_species = get_species(params, real_valid_species)
    assert choosen_species == "canola", "The function should return the species."


def test_get_species_valid(dummy_valid_species):
    params = {"species": "species1"}
    assert get_species(params, dummy_valid_species) == "species1"


def test_get_species_invalid(dummy_valid_species):
    params = {"species": "unknown"}
    with pytest.raises(ValueError):
        get_species(params, dummy_valid_species)


def test_get_mode_valid(dummy_valid_mode):
    params = {"mode": "mode1"}
    assert get_mode(params, dummy_valid_mode) == "mode1"


def test_get_mode_invalid(dummy_valid_mode):
    params = {"mode": "unknown"}
    with pytest.raises(ValueError):
        get_mode(params, dummy_valid_mode)


def test_get_age_valid(dummy_valid_age):
    params = {"age": "young"}
    assert get_age(params, dummy_valid_age) == "young"


def test_get_age_invalid(dummy_valid_age):
    params = {"age": "old"}
    with pytest.raises(ValueError):
        get_age(params, dummy_valid_age)


@pytest.mark.parametrize("params, valid_age_ranges, expected", [
    ({"age": "25"}, ["20-30"], "25"),
    ({"age": "17"}, ["15-18"], "17"),
    ({"age": "5"}, ["1-10"], "5"),
    ({"age": "65"}, ["60-70", "72", "75-80"], "65"),
    ({"age": "72"}, ["60-70", "72", "75-80"], "72"),
    ({"age": "30"}, ["25,26,27", "28-32"], "30"),
    ({"age": "26"}, ["25,26,27", "28-32"], "26"),
    ({"age": "28"}, ["25,26,27", "28-32"], "28"),
])
def test_get_age_valid_comprehensive(params, valid_age_ranges, expected):
    """Test get_age with valid age inputs comprehensively."""
    assert get_age(params, valid_age_ranges) == expected

@pytest.mark.parametrize("params, valid_age_ranges", [
    ({"age": "33"}, ["20-30"]),
    ({"age": "14"}, ["15-18"]),
    ({"age": "11"}, ["1-10"]),
    ({"age": "73"}, ["60-70", "72", "75-80"]),
    ({"age": "81"}, ["60-70", "72", "75-80"]),
    ({"age": "24"}, ["25,26,27", "28-32"]),
    ({"age": "33"}, ["25,26,27", "28-32"]),
    ({"age": "0"}, ["1-10"]),
    ({"age": "100"}, ["90-99"]),
])
def test_get_age_invalid_comprehensive(params, valid_age_ranges):
    """Test get_age with invalid age inputs comprehensively."""
    with pytest.raises(ValueError):
        get_age(params, valid_age_ranges)

@pytest.mark.parametrize("params, valid_age_ranges", [
    ({"age": "20"}, ["20-20"]),  # Edge case where start and end of range are the same
    ({"age": "20"}, ["20"]),  # Single age
    ({"age": "20"}, ["19,20,21"]),  # Comma-separated ages including the target
])
def test_get_age_edge_cases_comprehensive(params, valid_age_ranges):
    """Test get_age with edge cases comprehensively."""
    assert get_age(params, valid_age_ranges) == params["age"]


# Valid scenario: species="rice", mode="cylinder", age="3"
def test_get_age_valid_params(dataset_params, model_chooser_real_df):
    # get valid species, mode, and age ranges
    valid_species = model_chooser_real_df["species"].unique().tolist()
    valid_mode = model_chooser_real_df["mode"].unique().tolist()
    valid_age_ranges = model_chooser_real_df["age"].unique().tolist()
    valid_primary_model_ids = model_chooser_real_df["primary_model_id"].unique().tolist()
    
    species = get_species(dataset_params, valid_species)
    mode = get_mode(dataset_params, valid_mode)
    age = get_age(dataset_params, valid_age_ranges)
    primary_model_id = get_primary_model_id(dataset_params, valid_primary_model_ids)
    expected_path = "models/rice/younger/primary"
    assert (
        determine_primary_model_id(model_chooser_real_df, species, mode, age, primary_model_id)
        == expected_path
    )

# Valid scenario: Matching species, mode, and age
def test_valid_primary_model_id_real(model_chooser_real_df):
    # Example for a valid combination
    species = "soybean"
    mode = "cylinder"
    age = "3"  # Age within the range specified in the fixture
    primary_model_id = None
    expected_path = "models/soybean/primary"
    assert (
        determine_primary_model_id(model_chooser_real_df, species, mode, age, primary_model_id)
        == expected_path
    )


# Edge case: Species with different models for different ages
def test_edge_case_primary_model_id_for_different_ages(model_chooser_real_df):
    species = "rice"
    mode = "cylinder"
    young_age = "4"
    older_age = "9"
    young_primary_model_id = None
    older_primary_model_id = None
    young_expected = "models/rice/younger/primary"
    older_expected = None  # Older rice does not have a primary model path in the fixture data

    assert (
        determine_primary_model_id(model_chooser_real_df, species, mode, young_age, young_primary_model_id)
        == young_expected
    )
    assert (
        determine_primary_model_id(model_chooser_real_df, species, mode, older_age, older_primary_model_id)
        == older_expected
    )


# Invalid scenario: Parameters do not match any models
def test_invalid_primary_model_id_real(model_chooser_real_df):
    species = "unknown"
    mode = "unknown"
    age = "100"  # Unreasonably high age, not expected to match
    primary_model_id = None
    with pytest.raises(ValueError, match="No valid models found."):
        determine_primary_model_id(model_chooser_real_df, species, mode, age, primary_model_id)


# Scenario: Valid species and mode but age out of range
def test_valid_species_mode_but_age_out_of_range(model_chooser_real_df):
    species = "arabidopsis"
    mode = "plate"
    age = "6"  # Age not within the specified range for arabidopsis in plate mode
    primary_model_id = None
    with pytest.raises(ValueError, match="No valid models found."):
        determine_primary_model_id(model_chooser_real_df, species, mode, age, primary_model_id)


# Valid scenario for lateral model path
def test_valid_lateral_model_id(model_chooser_real_df):
    species = "canola"
    mode = "cylinder"
    age = "3"
    expected_path = "models/canola/lateral"
    lateral_model_id = None
    assert (
        determine_lateral_model_id(model_chooser_real_df, species, mode, age, lateral_model_id)
        == expected_path
    )


# No lateral model available for the given parameters
def test_no_lateral_model_found(model_chooser_real_df):
    species = "rice"
    mode = "cylinder"
    age = "5"  # Based on the fixture, rice does not have a lateral model for these parameters
    lateral_model_id = None
    assert determine_lateral_model_id(model_chooser_real_df, species, mode, age, lateral_model_id) == None


# Invalid scenario for lateral model path
def test_invalid_lateral_model_id(model_chooser_real_df):
    species = "unknown"
    mode = "unknown"
    age = "100"
    lateral_model_id = None
    with pytest.raises(ValueError, match="No valid models found."):
        determine_lateral_model_id(model_chooser_real_df, species, mode, age, lateral_model_id)


# Valid scenario for crown model path
def test_valid_crown_model_id(model_chooser_real_df):
    species = "rice"
    mode = "cylinder"
    age = "6"
    crown_model_id = None
    expected_path = "models/rice/older/crown"
    assert (
        determine_crown_model_id(model_chooser_real_df, species, mode, age, crown_model_id) == expected_path
    )


# No crown model available for the given parameters
def test_no_crown_model_found(model_chooser_real_df):
    species = "soybean"
    mode = "cylinder"
    age = "3"  # Based on the fixture, soybean does not have a crown model for these parameters
    crown_model_id = None
    assert determine_crown_model_id(model_chooser_real_df, species, mode, age, crown_model_id) == None


# Invalid scenario for crown model path
def test_invalid_crown_model_id(model_chooser_real_df):
    species = "unknown"
    mode = "unknown"
    age = "100"
    crown_model_id = None
    with pytest.raises(ValueError, match="No valid models found."):
        determine_crown_model_id(model_chooser_real_df, species, mode, age, crown_model_id)


def test_choose_species_invalid(real_valid_species):
    with pytest.raises(ValueError):
        params = {"species": "species1"}
        get_species(params, real_valid_species)


def test_choose_mode(input_dir, real_valid_modes):
    params = get_params(input_dir)
    choosen_mode = get_mode(params, real_valid_modes)
    assert choosen_mode == "cylinder", "The function should return the mode."


def test_choose_mode_invalid(real_valid_modes):
    params = {"mode": "invalid_mode"}
    with pytest.raises(ValueError):
        get_mode(params, real_valid_modes)


def test_choose_age(input_dir, real_valid_ages):
    params = get_params(input_dir)
    choosen_age = get_age(params, real_valid_ages)
    assert choosen_age == "7", "The function should return the age."


def test_choose_age_invalid(real_valid_ages):
    params = {"age": "invalid_age"}
    with pytest.raises(ValueError):
        get_age(params, real_valid_ages)


def test_determine_primary_model_id_valid(model_chooser):
    primary_model_id = determine_primary_model_id(
        model_chooser, "canola", "cylinder", "7", None
    )
    assert (
        primary_model_id
        == "/workspace/models/canola_pennycress_arabidopsis/primary/240130_075952.multi_instance.n=576"
    ), "The function should return the primary model id."


def test_determine_primary_model_id_both_present(primary_model_and_dataset_params_present, model_chooser_real_df):
    params = primary_model_and_dataset_params_present
    model_chooser = model_chooser_real_df
    species = get_species(params, model_chooser["species"].unique().tolist())
    mode = get_mode(params, model_chooser["mode"].unique().tolist())
    age = get_age(params, model_chooser["age"].unique().tolist())
    primary_model_id = get_primary_model_id(params, model_chooser["primary_model_id"].unique().tolist())
    found_primary_model_id = determine_primary_model_id(model_chooser, species, mode, age, primary_model_id)
    assert found_primary_model_id == params["primary_model_id"], "The function should return the primary model id."


def test_determine_primary_model_id_invalid(invalid_primary_model_and_dataset_params_present, model_chooser_real_df):
    params = invalid_primary_model_and_dataset_params_present
    model_chooser = model_chooser_real_df

    # Fetch species, mode, age without assuming they raise an error
    species = get_species(params, model_chooser["species"].unique().tolist())
    mode = get_mode(params, model_chooser["mode"].unique().tolist())
    age = get_age(params, model_chooser["age"].unique().tolist())

    # Expecting an error when checking for a valid primary_model_id
    with pytest.raises(ValueError):
        primary_model_id = get_primary_model_id(params, model_chooser["primary_model_id"].unique().tolist())


