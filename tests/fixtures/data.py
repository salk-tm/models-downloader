import pytest
import pandas as pd

from pathlib import Path


@pytest.fixture
def model_params():
    """Path to file with 7 day-old canola cylinder model parameters."""
    return Path("tests") / "data" / "model_params.json"


@pytest.fixture
def model_chooser():
    """Dataframe with standard model chooser table."""
    return pd.read_csv(Path("tests") / "data" / "model_chooser_table.csv")


@pytest.fixture
def input_dir():
    """Path to input directory."""
    return Path("tests") / "data"
