# models-downloader


## Description
The `main` function (found in "src/main.py") implements logic to choose trained SLEAP model(s) for predictions of roots. In the future this will interact with Bloom, but for now the current models can be found here: [models, verified 2024-05-02.](https://salkinstitute.box.com/s/2eawy98jpju176semogj9k7n6f9udl70). Refer to the [model chooser table, verified 2024-05-02](https://salkinstitute.box.com/s/89wvqr95x8n06pod7vnhv8e93iygflwy) in the folder to see which {species, mode, age} combinations and models are available.

## Arguments

- **`input_dir`**: The directory containing the necessary input files.
  - **`model_params.json`**: This file contains all the necessary parameters to select the appropriate model(s). The combination of "species", "mode", and "age" uniquely determines a set of models. However, you can directly specify model IDs for the lateral, primary, or crown models—these are currently paths in the container and will take precedence over the {species, mode, age} combination. The JSON file should maintain a consistent structure each time. Please ensure to include the following keys, and use null where any field is intentionally left empty:
    - `species`: valid species are "soybean", "canola", "pennycress", "arabidopsis", "rice".
    - `mode`: valid modes are "cylinder", "multiplant cylinder", "plate".
    - `age`: ages 2-14 days-old are allowed but are species-specific (see table).
    - Example:
      ```json
        {
            "species": "canola",
            "mode": "cylinder",
            "age": "7",
            "lateral_model_id": null,
            "primary_model_id": null,
            "crown_model_id": null
        }
      ```
  - **`*_models.zip`**: Directory with models that correspond to models in model chooser table. The main function searches for files with "_models.zip" in the end of the filename.  

- **`output_dir`**: The directory where the output files will be saved.
  - **zipped chosen models**: Models choosen by user parameters and model chooser table are copied as zip files to the output directory.
  - **`model_paths.csv`**: CSV of `model_type`, `model_id`, `model_path` corresponding to root type, *unique model id from Bloom in the future* and path of the model at it's destination. 


## Notes
- Ensure that the `input_dir` contains the current zipped models with the model chooser table inside and user defined parameters for choosing the model.
- Review and adjust the parameters in `model_params.json` as needed to run the correct models on the dataset.

## Badges
[![pipeline status](https://gitlab.com/salk-tm/models-downloader/badges/main/pipeline.svg)](https://gitlab.com/salk-tm/models-downloader/-/commits/main)

[![coverage report](https://gitlab.com/salk-tm/models-downloader/badges/main/coverage.svg)](https://gitlab.com/salk-tm/models-downloader/-/commits/main)

## Installation

**Make sure to have Docker Desktop running first**


You can pull the image if you don't have it built locally, or need to update the latest, with

```
docker pull registry.gitlab.com/salk-tm/models-downloader:latest
```

Then, to run the image interactively:

```
docker run -it registry.gitlab.com/salk-tm/models-downloader:latest bash
```

To run the image interactively with data mounted to the container, use the syntax

```
docker run -v /path/on/host:/path/in/container [other options] image_name [command]
```

For example: 

```
docker run -v C:\Users\eb\Desktop\root_phenotyping\sleap-roots-pipeline-dir\models_downloader\input:/workspace/input -
v C:\Users\eb\Desktop\root_phenotyping\sleap-roots-pipeline-dir\models_downloader\output:/workspace/output -it registry.gitlab.com/salk-tm/models-downloader:latest bash
```

Then you can run the `main` function in the /workspace directory in the container using

```
python src/main.py ./input ./output
```

and check "/workspace/output" for the expected files (also available on the mounted host output folder).

This can all be run via the command line as well:

```
docker run -v C:\Users\eb\Desktop\root_phenotyping\sleap-roots-pipeline-dir\models_downloader\input:/workspace/input -
v C:\Users\eb\Desktop\root_phenotyping\sleap-roots-pipeline-dir\models_downloader\output:/workspace/output -it registry.gitlab.com/salk-tm/models-downloader:latest python src/main.py ./input ./output
```

- The DockerFile installs the environment from `environment.yml` as the base environment on the container then copies "src" to the /workspace directory of the container. 


**Notes:**

- The `registry.gitlab.com` is the Docker registry where the images are pulled from. This is only used when pulling images from the cloud, and not necesary when building/running locally.
- `-it` ensures that you get an interactive terminal. The `i` stands for interactive, and `t` allocates a pseudo-TTY, which is what allows you to interact with the bash shell inside the container.
- The `-v` or `--volume` option mounts the specified directory with the same level of access as the directory has on the host.
- `bash` is the command that gets executed inside the container, which in this case is to start the bash shell.
- Order of operations is 1. Pull (if needed): Get a pre-built image from a registry. 2. Run: Start a container from an image.

## Support
contact Elizabeth at eberrigan@salk.edu

## Contributing
- Please make a new branch, starting with your name, with any changes, and request a reviewer before merging with the main branch since this container will be used by all HPI.
- Please document using the same conventions (docstrings for each function and class, typing-hints, informative comments).
- Tests are written in the pytest framework. Data used in the tests are defined as fixtures in "tests/fixtures/data.py" ("https://docs.pytest.org/en/6.2.x/reference.html#fixtures-api").

## Build
To build via automated CI, just push to `main`. See [`.gitlab-ci.yml`](.gitlab-ci.yml) for the runner definition.

To build locally for testing:

```
docker build --platform linux/amd64 --tag registry.gitlab.com/salk-tm/models-downloader:latest .
```
