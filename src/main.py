import json
import logging
import shutil
import sys
import zipfile
import pandas as pd

from pathlib import Path

from src.log_util import setup_logging, StreamToLogger
from src.model_chooser import (
    get_params,
    get_model_ids,
    get_species,
    get_mode,
    get_age,
    get_crown_model_id,
    get_lateral_model_id,
    get_primary_model_id,
)


def main(input_dir: str, output_dir: str) -> pd.DataFrame:
    """Main function to choose and copy model files based on user parameters.

    The function performs the following steps:
    - Reads user parameters for model selection from the input directory.
    - Unzips the "*_models.zip" folder in the input directory.
    - Reads a model chooser table in the "*_models" folder
    - Determines appropriate model IDs based on the user parameters and the model chooser table.
    - Copies the selected model ZIP files to the output directory while preserving their names.
    - Generates and saves a DataFrame that captures the paths and identifiers of the copied model files.

    Args:
        input_dir: The path to the directory containing the models and user parameters.
        output_dir: The path to the directory where the output, including copied model files, will be stored.

    Returns:
        pd.DataFrame: A DataFrame containing columns for model types, identifiers, and paths to the copied files
            in the output directory.
    """

    input_dir = Path(input_dir)
    output_dir = Path(output_dir)

    # Setup logging
    setup_logging(output_dir)

    # Get user parameters dict for choosing model(s)
    user_params = get_params(input_dir)
    logging.info(f"Parameters: {user_params}")

    # Find models folder in the input directory and unzip it
    models = unzip_models(input_dir)
    logging.info(f"Models folder: {models}")

    # Get model chooser table from the models folder
    model_chooser_path = Path(models) / "model_chooser_table.xlsx"
    model_chooser_df = pd.read_excel(model_chooser_path, engine="openpyxl")
    logging.info(f"Model chooser table: {model_chooser_path}")

    # Get the model ids dict for the chosen models using the model chooser dataframe
    model_ids = get_model_ids(user_params, model_chooser_df)
    logging.info(f"Model ids: {model_ids}")

    # Initialize a dictionary to store the paths and ids of the chosen models
    model_paths = {"primary": {}, "lateral": {}, "crown": {}}
    # Build paths for the zipped chosen models
    if model_ids["primary"] is not None:
        primary_model_path = Path(models) / f"{model_ids['primary']}.zip"
        model_paths["primary"]["model_path"] = primary_model_path
        model_paths["primary"]["model_id"] = model_ids["primary"]
    if model_ids["lateral"] is not None:
        lateral_model_path = Path(models) / f"{model_ids['lateral']}.zip"
        model_paths["lateral"]["model_path"] = lateral_model_path
        model_paths["lateral"]["model_id"] = model_ids["lateral"]
    if model_ids["crown"] is not None:
        crown_model_path = Path(models) / f"{model_ids['crown']}.zip"
        model_paths["crown"]["model_path"] = crown_model_path
        model_paths["crown"]["model_id"] = model_ids["crown"]

    # Find the models in the models folder and copy them to the output directory if they
    # exist
    copied_model_paths = copy_model_files(model_paths, output_dir)
    logging.info(f"Copied model paths: {copied_model_paths}")

    # Make a csv file with the model ids and the chosen model paths for each model type
    model_paths_df = copied_models_dict_to_dataframe(copied_model_paths)

    # Save the DataFrame to a CSV file
    model_paths_df.to_csv(output_dir / "model_paths.csv", index=False)
    logging.info(f"Model paths DataFrame saved to {output_dir / 'model_paths.csv'}")

    return model_paths_df


def unzip_models(input_dir: str) -> Path:
    """Unzips a single '*_models.zip' file located in the input directory.

    Args:
        input_dir: The directory to search and unzip the ZIP file.

    Returns:
        Path: Path object pointing to the directory of the unzipped contents.

    Raises:
        FileNotFoundError: If no ZIP file is found.
        ValueError: If more than one ZIP file is found.
    """
    input_dir = Path(input_dir)  # Ensure input_dir is a Path object
    zip_files = list(input_dir.glob("*_models.zip"))  # Search for ZIP files

    if len(zip_files) == 1:
        zip_file = zip_files[0]
        output_folder = input_dir / zip_file.stem  # Initially proposed output folder

        with zipfile.ZipFile(zip_file, "r") as zip_ref:
            zip_ref.extractall(output_folder.parent)
            print(f"Unzipped {zip_file.name} successfully to {output_folder}")
            return output_folder

    elif len(zip_files) > 1:
        raise ValueError("More than one '*_models.zip' file found in the directory.")
    else:
        raise FileNotFoundError("No '*_models.zip' file found in the directory.")


def copy_model_files(model_paths: dict, output_dir: str) -> dict:
    """Copies model zip files from their current locations to the output directory.

    Args:
        model_paths: A dictionary where keys are model types and values are
            dictionaries containing 'model_path' (Path to the model zip files) and
            'model_id'.
        output_dir: The directory where the models should be copied.

    Returns:
        dict: A dictionary where keys are model types and values are dictionaries with
            'model_path' (Paths to the copied model zip files) and 'model_id'.

    Raises:
        FileNotFoundError: If a specified model file does not exist.
    """
    # Ensure output_dir is a Path object
    output_dir = Path(output_dir)
    # Initialize a dictionary to store the output paths of the copied models
    copied_model_paths = {}

    for model_type, info in model_paths.items():
        # Safely get the model path or None if not present
        model_path = info.get("model_path")
        if model_path:
            model_path = Path(model_path)
            # Check if the model file exists
            if not model_path.exists():
                raise FileNotFoundError(f"Model file not found: {model_path}")

            # Construct the full path for the output
            model_output_path = output_dir / model_path.name
            # Copy the model file to the output directory
            shutil.copy(model_path, model_output_path)
            print(f"Copied {model_path} to {model_output_path}")

            # Store the output path relative to the output folder along with the
            # model_id in the copied_model_paths dictionary for the model type
            copied_model_paths[model_type] = {
                "model_path": model_path.name,
                "model_id": info.get(
                    "model_id"
                ),  # Safely get model_id or None if not present
            }
        else:
            print(f"No model path provided for {model_type}, skipping.")

    return copied_model_paths


def copied_models_dict_to_dataframe(copied_model_paths: dict) -> pd.DataFrame:
    """Converts a dictionary of model paths into a pandas DataFrame.

    Args:
        copied_model_paths: A dictionary where keys are model types and values are
            dictionaries containing 'model_path' and 'model_id'.

    Returns:
        DataFrame: A pandas DataFrame with columns for 'model_type', 'model_id', and
            'model_path'.
    """
    # Prepare data for DataFrame conversion
    data = []
    for model_type, details in copied_model_paths.items():
        data.append(
            {
                "model_type": model_type,
                "model_id": details["model_id"],
                "model_path": details["model_path"],
            }
        )

    # Create DataFrame
    df = pd.DataFrame(data)
    return df


if __name__ == "__main__":
    input_dir = sys.argv[1]
    output_dir = sys.argv[2]
    main(input_dir, output_dir)
