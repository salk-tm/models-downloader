import glob
import json
import shutil
import zipfile
import pandas as pd
import numpy as np
import logging

from pathlib import Path
from typing import List, Optional, Tuple


def get_params(input_dir: str) -> dict:
    """Read the parameters from the input directory from the `model_params.json` file.

    Args:
        input_dir: Path to the input directory. This directory should contain
            a `model_params.json` file which is used to identify the model.

    Returns:
        dict: A dictionary containing the identifiying characteristics for a model.

    """
    params = {}
    params_file = Path(input_dir) / "model_params.json"
    if params_file.exists():
        with open(params_file, "r") as f:
            params = json.load(f)
        return params
    else:
        raise FileNotFoundError(f"model_params.json not found in {input_dir}")


def get_species(params: dict, valid_species: List[str]) -> str:
    """Get the species name from the params dict.

    Args:
        params: A dictionary containing the species name.
        valid_species: A list of valid species names.

    Returns:
        str: The species name or None.

    """
    # get the species name
    species = params.get("species")
    # check if the species name is valid
    if species is not None and species not in valid_species:
        raise ValueError(f"Invalid species name: {species}")
    return species


def get_mode(params: dict, valid_modes: List[str]) -> str:
    """Get the mode from the params dict.

    Args:
        params: A dictionary containging the mode.
        valid_mode: A list of valid mode names for the type of image(s).
    Returns:
        str: The mode or None.

    """
    # get the mode
    mode = params.get("mode")
    # check if the mode is valid
    if mode is not None and mode not in valid_modes:
        raise ValueError(f"Invalid mode: {mode}")
    return mode


def get_age(params: dict, valid_age_ranges: List[str]) -> Optional[str]:
    """Get the age from the params dict.

    Args:
        params: A dictionary containing the age.
        valid_age_ranges: A list of valid ages or age ranges (as strings).
    Returns:
        Optional[str]: The age or None.

    """
    # get the age from params
    age = params.get("age")

    # Parse the valid ages into a set of individual ages
    valid_ages = set()
    for age_range in valid_age_ranges:
        if "," in age_range:
            ages = [a.strip() for a in age_range.split(",")]
            valid_ages.update(ages)
        elif "-" in age_range:
            start, end = map(int, map(str.strip, age_range.split("-")))
            valid_ages.update(str(a) for a in range(start, end + 1))
        else:
            valid_ages.add(age_range.strip())

    # Check if the age is valid
    if age is not None:
        age = age.strip()
        if age not in valid_ages:
            raise ValueError(f"Invalid age: {age}. Valid ages are {valid_ages}")
    return age


def get_primary_model_id(params: dict, valid_primary_model_ids: List[str]) -> str:
    """Get the primary model id from the params dict.

    Args:
        params: A dictionary containing the primary model id.
        valid_primary_model_ids: A list of valid primary model ids.

    Returns:
        str: The primary model id or None.

    """
    # get the primary model id
    primary_model_id = params.get("primary_model_id")

    # check if the primary model id is valid
    if primary_model_id is not None and primary_model_id not in valid_primary_model_ids:
        raise ValueError(f"Invalid primary model id: {primary_model_id}")
    return primary_model_id


def get_lateral_model_id(params: dict, valid_lateral_model_ids: List[str]) -> str:
    """Get the lateral model id from the params dict.

    Args:
        params: A dictionary containing the lateral model id.
        valid_lateral_model_ids: A list of valid lateral model ids.
    Returns:
        str: The lateral model id or None.

    """
    # get the lateral model id
    lateral_model_id = params.get("lateral_model_id")
    # check if the lateral model id is valid
    if lateral_model_id is not None and lateral_model_id not in valid_lateral_model_ids:
        raise ValueError(f"Invalid lateral model id: {lateral_model_id}")
    return lateral_model_id


def get_crown_model_id(params: dict, valid_crown_model_ids: List[str]) -> str:
    """Get the crown model id from the params dict.

    Args:
        params: A dictionary containing the crown model id.
        valid_crown_model_ids: A list of valid crown model ids.
    Returns:
        str: The crown model id or None.

    """
    # get the crown model id
    crown_model_id = params.get("crown_model_id")
    # check if the crown model id is valid
    if crown_model_id is not None and crown_model_id not in valid_crown_model_ids:
        raise ValueError(f"Invalid crown model id: {crown_model_id}")
    return crown_model_id


def determine_primary_model_id(
    model_chooser: pd.DataFrame,
    species: Optional[str],
    mode: Optional[str],
    age: Optional[str],
    primary_model_id: Optional[str],
) -> str:
    """Determine the id of the primary root model for predictions.

    Args:
        model_chooser: DataFrame for choosing model by model_id.
        species: Species name from params.
        mode: Mode of images from params.
        age: Age of plants from params.
        primary_model_id: Primary model identifier from params.
    Returns:
        primary_model_id from the model_chooser dataframe.

    """
    # Check if the primary model id is specified in the params
    if primary_model_id is not None:
        return primary_model_id

    # If the `primary_model_id` is not provided in the params,
    # determine the primary model id based on the species, mode, and age
    # Filter the model params dataframe
    filtered_df = model_chooser[
        (model_chooser["species"] == species)
        & (model_chooser["mode"] == mode)
        & (model_chooser["age"].str.contains(f"\\b{age}\\b"))
    ]

    # Check if there are any valid models
    if filtered_df.empty:
        raise ValueError("No valid models found.")

    # Filter for valid models with a specified primary model id
    primary_models_df = filtered_df.dropna(subset=["primary_model_id"])

    # Check if there are any valid primary models
    if primary_models_df.empty:
        print(
            "No primary predictions expected for these images due to the expected"
            "absence of a primary model."
        )
        return None

    # Check if there are multiple primary models
    if len(primary_models_df) > 1:
        raise ValueError(
            f"Multiple primary models found: {primary_models_df['primary_model_id'].tolist()}"
        )

    # Get the primary root model id
    primary_model_id = primary_models_df["primary_model_id"].values[0]

    return primary_model_id


def determine_lateral_model_id(
    model_chooser: pd.DataFrame,
    species: Optional[str],
    mode: Optional[str],
    age: Optional[str],
    lateral_model_id: Optional[str],
) -> str:
    """Determine the id of the lateral root model for predictions.

    Args:
        model_chooser: DataFrame for choosing model by model_id.
        species: Species name from params.
        mode: Mode of images from params.
        age: Age of plants from params.
        lateral_model_id: Lateral model identifier from params.

    Returns:
        lateral_model_id from the model_chooser dataframe.

    """
    # Check if the lateral model id is specified in the params
    if lateral_model_id is not None:
        return lateral_model_id

    # If the `lateral_model_id` is not provided in the params,
    # determine the lateral model id based on the species, mode, and age
    # Filter the model params dataframe
    filtered_df = model_chooser[
        (model_chooser["species"] == species)
        & (model_chooser["mode"] == mode)
        & (model_chooser["age"].str.contains(f"\\b{age}\\b"))
    ]

    # Check if there are any valid models
    if filtered_df.empty:
        raise ValueError("No valid models found.")

    # Filter for valid models with a specified lateral model id
    lateral_models_df = filtered_df.dropna(subset=["lateral_model_id"])

    # Check if there are any valid lateral models
    if lateral_models_df.empty:
        print(
            "No lateral predictions expected for these images due to the expected"
            "absence of a lateral model."
        )
        return None

    # Check if there are multiple lateral models
    if len(lateral_models_df) > 1:
        raise ValueError(
            f"Multiple lateral models found: {lateral_models_df['lateral_model_id'].tolist()}"
        )

    # Get the lateral root model id
    lateral_model_id = lateral_models_df["lateral_model_id"].values[0]

    return lateral_model_id


def determine_crown_model_id(
    model_chooser: pd.DataFrame,
    species: Optional[str],
    mode: Optional[str],
    age: Optional[str],
    crown_model_id: Optional[str],
) -> str:
    """Determine the id of the crown root model for predictions.

    Args:
        model_chooser: DataFrame for choosing model by model_id.
        species: Species name from params.
        mode: Mode of images from params.
        age: Age of plants from params.
        crown_model_id: Crown model identifier from params.

    Returns:
        crown_model_id from the model_chooser dataframe.

    """
    # Check if the crown model id is specified in the params
    if crown_model_id is not None:
        return crown_model_id

    # If the `crown_model_id` is not provided in the params,
    # determine the crown model id based on the species, mode, and age
    # Filter the model params dataframe
    filtered_df = model_chooser[
        (model_chooser["species"] == species)
        & (model_chooser["mode"] == mode)
        & (model_chooser["age"].str.contains(f"\\b{age}\\b"))
    ]

    # Check if there are any valid models
    if filtered_df.empty:
        raise ValueError("No valid models found.")

    # Filter for valid models with a specified crown model id
    crown_models_df = filtered_df.dropna(subset=["crown_model_id"])

    # Check if there are any valid crown models
    if crown_models_df.empty:
        print(
            "No crown predictions expected for these images due to the expected"
            "absence of a crown model."
        )
        return None

    # Check if there are multiple crown models
    if len(crown_models_df) > 1:
        raise ValueError(
            f"Multiple crown models found: {crown_models_df['crown_model_id'].tolist()}"
        )

    # Get the crown root model id
    crown_model_id = crown_models_df["crown_model_id"].values[0]

    return crown_model_id


def get_model_ids(model_params: dict, model_chooser: pd.DataFrame) -> dict:
    """Get the model ids for the chosen models from the model chooser dataframe.

    Args:
        model_params: A dictionary containing the identifying characteristics for a model.
        model_chooser: A pandas dataframe containing the model chooser table.

    Returns:
        dict: A dictionary of model type and model id pairs.
    """
    # Initialize list for model ids
    model_ids = {}

    # Get valid model parameters from the model chooser table
    valid_species = model_chooser["species"].unique().tolist()
    print(f"valid species: {valid_species}")
    valid_mode = model_chooser["mode"].unique().tolist()
    print(f"valid modes: {valid_mode}")
    valid_age_ranges = model_chooser["age"].unique().tolist()
    print(f"valid ages: {valid_age_ranges}")
    valid_primary_model_ids = model_chooser["primary_model_id"].unique().tolist()
    print(f"valid primary model ids: {valid_primary_model_ids}")
    valid_lateral_model_ids = model_chooser["lateral_model_id"].unique().tolist()
    print(f"valid lateral model ids: {valid_lateral_model_ids}")
    valid_crown_model_ids = model_chooser["crown_model_id"].unique().tolist()
    print(f"valid crown model ids: {valid_crown_model_ids}")

    # Get the user parameters for choosing the model(s)
    species = get_species(model_params, valid_species)
    print(f"species is {species}")
    mode = get_mode(model_params, valid_mode)
    print(f"mode is {mode}")
    age = get_age(model_params, valid_age_ranges)
    print(f"age is {age}")
    primary_model_id = get_primary_model_id(model_params, valid_primary_model_ids)
    print(f"primary model id is {primary_model_id}")
    lateral_model_id = get_lateral_model_id(model_params, valid_lateral_model_ids)
    print(f"lateral model id is {lateral_model_id}")
    crown_model_id = get_crown_model_id(model_params, valid_crown_model_ids)
    print(f"crown model id is {crown_model_id}")

    # Determine the model ids for the chosen models
    final_primary_model_id = determine_primary_model_id(
        model_chooser, species, mode, age, primary_model_id
    )
    print(f"chosen primary model id: {final_primary_model_id}")
    model_ids["primary"] = final_primary_model_id

    final_lateral_model_id = determine_lateral_model_id(
        model_chooser, species, mode, age, lateral_model_id
    )
    print(f"chosen lateral model id is {final_lateral_model_id}")
    model_ids["lateral"] = final_lateral_model_id

    final_crown_model_id = determine_crown_model_id(
        model_chooser, species, mode, age, crown_model_id
    )
    print(f"chosen crown model id is {final_crown_model_id}")
    model_ids["crown"] = final_crown_model_id

    return model_ids
